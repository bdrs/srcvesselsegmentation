#include <iostream>
#include "functions.h"

/**

Funzione di servizio per impostazione di parametri per lineOperator
@int   ortsize;
@float _tollerance;

*/
void line::service (int ortsize, float _tollerance) {
	line::ORTSIZE = ortsize;
	line::tollerance = _tollerance;
}


/**

funzione per la rimozione del rumore relativo al bordo della retina
@cv::Mat &imgIni;	  immagine di ingresso 
@int sizeErosion;	  dimensionamento SE per erosione
@cv::Mat &imgToErode; immagine di uscita

*/
void utility::removeBorder (cv::Mat & imgIni, int sizeErosion, cv::Mat & imgToErode) {
		
		std::vector<cv::Mat> channels;
		cv::split(imgIni,channels);
		cv::Mat imgRGB = channels[0] + channels[1] + channels[2];
		cv::Mat imgRgb100;
		cv::threshold(imgRGB,imgRgb100,100,255,CV_THRESH_BINARY_INV);
		imgRGB -= imgRgb100;


		cv::Mat bwmask;
		bwmask=imgRGB-channels[0]  ;
		
		cv::threshold(bwmask,bwmask,1,255,CV_THRESH_BINARY);
		
		cv::morphologyEx(bwmask,bwmask,CV_MOP_ERODE,cv::getStructuringElement(CV_SHAPE_ELLIPSE,cv::Size(sizeErosion,sizeErosion)));

		imgToErode = imgToErode & bwmask;

		return;
	}

/**

funzione per generazione di una ROI
@const cv::Mat &input; immagine ingresso
@cv::Point2i   center; centro della ROI
@int		   size;   dimenisone della ROI

*/
cv::Mat utility::selectRoiSquare (const cv::Mat & input, cv::Point2i center, int size)
	{

	    return ( input(cv::Rect(center.x-size/2,center.y-size/2,size,size)) );

	}

/**

funzione per il calcolo del livello medio di grigio dell'immagine in ingresso
valore restituito: float; livello medio di grigio
@const cv::Mat &input; immagine in ingresso

*/
float utility::avgGrayLevel (const cv::Mat & input)
{
    float avg = 0;

    for (int y = 0; y < input.rows; y++)
	{
	const unsigned char * yThRow = input.ptr<unsigned char>(y);
	for (int x = 0; x < input.cols; x++)
		{
		    avg += float(yThRow[x]);

		}
	}
	
	return avg/(input.rows*input.cols);

}

/**

funzione per il calcolo del livello medio di grigio a partire da un set di punti
valore restituito: float; intesit� media 
@const cv::Mat &input; immagine in ingresso
@const std::vector<cv::Point2i> &points; vettore di punti per il calcolo del livello medio di grigio

*/
float utility::avgOnPoints ( const cv::Mat & input, const std::vector<cv::Point2i> & points )
{
    float avg = 0;
	const unsigned char * rowPointer;
	
	for (int i = 0; i < points.size(); i++)
	{
	    rowPointer = input.ptr<unsigned char>(points[i].y);
		avg += float(rowPointer[points[i].x]);
	}

	return (avg/points.size());
	
}

/**

binarizzazione tramite percentile
@cv::Mat			&input;  immagine di partenza
@cv::Mat			&output; immagine in uscita
@const unsigned int lowPerc; limite inferiore del range di binarizzazione
@const unsigned int upPerc;	 limite superiore del range di binarizzazione

*/
void utility::percentileBinarization(cv::Mat & input, cv::Mat & output,const unsigned int lowPerc, const unsigned int upPerc)
{
    int lowValue, upValue;
    double totSamples = (double) input.rows*input.cols;
    std::vector<int> histogram = ucas::histogram(input);
    bool found = false;

    output = input.clone();
    output.setTo(cv::Scalar(0));

    int i = 0;
    unsigned int acc = 0; 

    while(!found && i < histogram.size())
    {
        
        acc += histogram[i];
        
        if ((acc/totSamples)*100 >= lowPerc)
        {
           lowValue = i;
           found = true;
        }
        i++;
    }

    found = false;

    while(!found && i < histogram.size())
    {
        acc += histogram[i];

        if ((acc/totSamples)*100 >= upPerc)
        {
           upValue = i;
           found = true;
        }
        i++;
    }

    for (int y = 0; y < input.rows; y++)
    {
        unsigned char * yThRowIn = input.ptr<unsigned char>(y);
        unsigned char * yThRowOut = output.ptr<unsigned char>(y);
         
        for (int x = 0; x < input.cols; x++)
        {
            if ( yThRowIn[x] >= lowValue && yThRowIn[x] <= upValue)
            {
                yThRowOut[x] = 255;
            }
        }
    }

    
}

/**

funzione per l'esecuzione dell'operazione morfologica "topHat"
@const cv::Mat &inputImg;  immagine di partenza
@cv::Mat	   &outputImg; immagine in cui viene memorizzato il risultato della topHat
@cv::Size	   strElSize;  dimensionamento dello SE per la morfologia

*/
void utility::topHatTransform (const cv::Mat & inputImg, cv::Mat & outputImg, int nRot, cv::Size strElSize) {

		
		outputImg = inputImg.clone();

		outputImg.setTo(cv::Scalar(0));
		std::vector<cv::Mat> strElement = createTiltedStructuringElements(strElSize.width,strElSize.height,nRot);

		cv::Mat topHat;
		for(int i=0; i<nRot; i++)
		{
			cv::morphologyEx(inputImg, topHat, cv::MORPH_TOPHAT, strElement[i]);	
			outputImg += topHat;
		}

		return;
	}

/**

funzione di servizio per la generazione delle maschere per i dataset che ne sono sprovvisti (STARE)

*/
void utility::generateMask () {

		std::vector<cv::Mat> images = aia::getImagesInFolder(std::string(DATASET_PATH)+"/STARE/images",".ppm");
	
		for(int i=0; i<images.size();i++) {
	
		cv::Mat channels[3];
		cv::split(images[i], channels);
		cv::Mat imgRgb100;
		cv::Mat imgRGB =  channels[0] + channels[1] + channels[2];
		cv::threshold(imgRGB,imgRgb100,100,255,CV_THRESH_BINARY_INV);
		
		imgRGB -= imgRgb100;

		cv::Mat bwmask;
		bwmask=channels[0] - imgRGB;
		cv::threshold(255-bwmask,bwmask,254,255,CV_THRESH_BINARY);
	
		std::string str = std::to_string(i+1);
		if(i<9)
		str = "0" + str + "_mask";
		else
		str = str + "_mask";

		ucas::imwrite(std::string(DATASET_PATH)+"/STARE/masks/" + str +".png",bwmask);
		}
		
	}

	// create 'n' rectangular Structuring Elements (SEs) at different orientations spanning the whole 360�
	cv::vector<cv::Mat>						// vector of 'width' x 'width' uint8 binary images with non-black pixels being the SE
	utility::createTiltedStructuringElements(
	int width,							// SE width (must be odd)
	int height,							// SE height (must be odd)
	int n)								// number of SEs
	throw (ucas::Error)
{
	// check preconditions
	if( width%2 == 0 )
		throw ucas::Error(ucas::strprintf("Structuring element width (%d) is not odd", width));
	if( height%2 == 0 )
		throw ucas::Error(ucas::strprintf("Structuring element height (%d) is not odd", height));

	// draw base SE along x-axis
	cv::Mat base(width, width, CV_8U, cv::Scalar(0));
	// workaround: cv::line does not work properly when thickness > 1. So we draw line by line.
	for(int k=width/2-height/2; k<=width/2+height/2; k++)
		cv::line(base, cv::Point(0,k), cv::Point(width, k), cv::Scalar(255));

	// compute rotated SEs
	cv::vector <cv::Mat> SEs;
	SEs.push_back(base);
	double angle_step = 180.0/n;
	for(int k=1; k<n; k++)
	{
		cv::Mat SE;
		cv::warpAffine(base, SE, cv::getRotationMatrix2D(cv::Point2f(base.cols/2.0f, base.rows/2.0f), k*angle_step, 1.0), cv::Size(width, width), CV_INTER_NN);
		SEs.push_back(SE);
	}

	return SEs;	 
}

/**

funzione per l'elaborazione dell'area contenuta dal cerchio di raggio radious e centro individuato 
dal punto di intensit� maggiore dell'immagine equalized tramite combinazione di due immagini (smooted & morf);
la sezione elaborata viene sostiuita nell'immagine di destinazione (lineOperated)

@cv::Mat &equalized;	immagine in ingresso in cui � enfatizzata l'area del disco ottico
@double  radious;		raggio della circonferenza che circoscrive l'area di lavoro
@cv::Mat &morf;			immagine di input ottenuta tramite operazioni morfologiche
@cv::Mat &smooted;		immagine di input ottenuta tramite applicazione di CLAHE
@cv::Mat &lineOperated; immagine di output originariamente ottenuta tramite lineOperator

*/
void utility::computeDisk ( cv::Mat & equalized, double radius,  cv::Mat & morf,  cv::Mat & smooted,   cv::Mat & lineOperated) {

		cv::Mat app = morf & smooted;

		cv::Point maxPoint;
		double maxValuue = 0;

		cv::minMaxLoc(equalized,NULL,&maxValuue,NULL,&maxPoint);
		
		int colorRef = 15;
		cv::circle(lineOperated,maxPoint,radius,cv::Scalar(colorRef),CV_FILLED);

		for(int y=0; y<lineOperated.rows; y++) {
			unsigned char * yCcol = lineOperated.ptr<unsigned char>(y);
			unsigned char * yABcol = app.ptr<unsigned char>(y);
			for (int x = 0; x<lineOperated.cols; x++) {
				if(yCcol[x] == colorRef)
					yCcol[x] = yABcol[x];
			}
		}


	}

/**

funzione che realizza la somma delle immagini nuova e morf per tutti i pixel che NON si trovano all'interno del cerchio di raggio radious e avente
centro nel punto di intensit� maggiore dell'immagine equalized
@double	 radius;	 raggio della circonferenza che circoscrive l'area da non elaborare
@cv::Mat &equalized; immagine equalizzata per la ricerca del punto di massima intensit�
@cv::Mat &morf;		 immagine di input ottenuta tramite operazione morfologica (topHat)
@cv::Mat &nuova;	 immagine di input basata sull'elaborazione di lineOperator e con porzione del disco ottico correta
@cv::Mat &sommaOut;	 immagine di output 

*/
void utility::addWithoutDisk (double radius, cv::Mat & equalized, cv::Mat & morf, cv::Mat & nuova, cv::Mat & sommaOut) {

		cv::Point maxPoint;
		double maxValuue = 0;

		cv::Mat service=morf.clone();
		service.setTo(cv::Scalar(0));

		cv::minMaxLoc(equalized,NULL,&maxValuue,NULL,&maxPoint);
		
		int colorRef = 15;
		cv::circle(service,maxPoint,radius,cv::Scalar(colorRef),CV_FILLED);

		for(int y=0; y<service.rows; y++) {
			unsigned char * yServiceCol = service.ptr<unsigned char>(y);
			unsigned char * yMorfCol = morf.ptr<unsigned char>(y);
			unsigned char * yNuovaCol = nuova.ptr<unsigned char>(y);
			unsigned char * ySommaOutCol = sommaOut.ptr<unsigned char>(y);
			for (int x = 0; x<service.cols; x++) {
				if(yServiceCol[x] != colorRef)
					ySommaOutCol[x] = yNuovaCol[x] + yMorfCol[x];
				else
					ySommaOutCol[x] =  yNuovaCol[x];
			}
		}
	
		return;
	}

/**

Funzione principale per ricerca dei vasi, viene sfruttata la classe lineOperator
@const cv::Mat &input;	    immagine di partenza
@cv::Mat	   &response;   immagine in uscita
@int		   degreesStep; risoluzione angolo di inclinazione delle linee 
@int		   lineLenght;	lunghezza linea ricercata

*/
void line::lineStrenght (const cv::Mat & input, cv::Mat & rensponse, int degreesStep, int lineLenght)
{
    float L, Lo, N;
	rensponse = cv::Mat (input.rows,input.cols,CV_32F,cv::Scalar(0)) ;

	int borderOffset = lineLenght/2;
	line::lineOperator * lineOp = new line::lineOperator(cv::Point2i(0,0),degreesStep,lineLenght);

	for ( int y = borderOffset; y < input.rows - borderOffset; y++ )
	{
	    float * yThRowOutput = rensponse.ptr<float>(y);
	    for ( int x = borderOffset; x < input.cols - borderOffset; x++ )
		{
		    lineOp->setCenter(cv::Point2i(x,y));

			N = utility::avgGrayLevel(utility::selectRoiSquare(input,cv::Point2i(x,y),lineLenght));

			for ( int w = 0; w < lineOp -> getLinesNumb(); w++ )
			{

			    lineOp->setLinePoint(w,input);

				L = utility::avgOnPoints(input,lineOp->getLinePoints());
				Lo = utility::avgOnPoints(input,lineOp->getOrthon());
			
				if (utility::approxCheck(Lo,L,tollerance))
				{
				    yThRowOutput[x] = std::max(yThRowOutput[x],(L-N));
				} else
				{
					yThRowOutput[x] = 0;
				}

			}

		}
	}

}

/**

funzione per selezionare un set di punti appartenenti alla retta ortogonale alla linea corrente
@int startPointx;  punto iniziale lungo l'ascissa
@int startPointy;  punto iniziale lungo l'ordinata
@int endPointx;    punto finale lungo l'ascissa
@int endPointy;    punto finale lungo l'ordinata

*/
int * line::getOrthonomalLine (int startPointx,int startPointy,int endPointx,int endPointy)
{
    int * orthoPoints = new int[4];

	int length = ORTSIZE;
	
    float vX = float(endPointx-startPointx);
    float vY = float(endPointy-startPointy);

    float mag = std::sqrt(vX*vX + vY*vY);
    vX = vX / mag;
    vY = vY / mag;
    float temp = vX;
    vX = 0-vY;
    vY = temp;
	orthoPoints[0] = line::round(endPointx + vX * length); //cX
    orthoPoints[1] = line::round(endPointy + vY * length); //cY
    orthoPoints[2] = line::round(endPointx - vX * length); //dX
    orthoPoints[3] = line::round(endPointy - vY * length); //dY

    return orthoPoints;
}


/**

funzione per verificare se x rientra nell'intervallo [y-percentage;y+percentage]
@float x;		   parametro da verificare
@float y;		   centro dell'intervallo
@float percentage; offset intervallo

*/
bool utility::approxCheck (float x, float y, float percentage)
{
    return ( x >= (y-y*percentage/100) && x <= (y*percentage/100+y) ) ? true : false;
}

/**

funzione per la rimozione dell'aree presenti nell'immagine binaria in ingresso con area minore di areaT
@cv::Mat &binInput; immagine binaria in ingresso
@double  areaT;		dimensione dell'area minima

*/
void utility::removeSmallAreas (cv::Mat & binInput, double areaT )
{
    cv::Mat inputCopy = binInput.clone();
	std::vector<std::vector<cv::Point>> contours;
	cv::findContours(	inputCopy, contours,cv::RETR_LIST,CV_CHAIN_APPROX_NONE);
	double area;
	for ( int i = 0; i < contours.size(); i++)
	{
	    area = cv::contourArea(contours[i]);
		
		if ( area <= areaT )
		{
		    cv::drawContours(binInput,contours,i,cv::Scalar(0),CV_FILLED);
		}
	}

}

/**

funzione per conversione di un float all'intero pi� vicino
@const float &toRound; numero in ingresso da approssimare

*/
int line::round (const float & toRound )
{
    float decimal;
	float integer;
	
	//estrazione parte decimale
	decimal = std::modf (toRound, &integer);

	return (decimal <= 0.5) ? int (integer) : int (integer+1);
}

/**
calcolo un numero di punti pari a lenght che giacciono sulla linea retta numero numberOfLine
@numberOLine; numero della linea calcolato in senso antiorario rispetto alla risoluzione dei gradi
@cv::Mat &input; immagine sulla quale il lineOperator � instanziato
*/
void line::lineOperator::setLinePoint (  int numberOfLine, const cv::Mat & input )
{
    
	int lineAngle = numberOfLine * this->degreesStep;
	float m = std::tan((float(lineAngle)*ucas::PI)/180.0);

	int startPointx,startPointy;
	int endPointx,endPointy;
    
	if ( m >= 0 && m <= 1)
	{
	    startPointx = this->center.x - this->lenght/2;
		startPointy = round((m*-1*this->lenght/2)+this->center.y);
		endPointx = this->center.x + this->lenght/2;
		endPointy = round((m*this->lenght/2)+this->center.y);
	} else if ( m >= 1 )
	{
	    startPointy = this->center.y - this->lenght/2;
		startPointx = round(((-1*this->lenght/2)/m)+this->center.x);
		endPointy = this->center.y + this->lenght/2;
		endPointx = round(((this->lenght/2)/m)+this->center.x);

	} else if ( m <= -1 )
	{
	    startPointy = this->center.y - this->lenght/2;
		startPointx = round(((-1*this->lenght/2)/m)+this->center.x);
		endPointy = this->center.y + this->lenght/2;
		endPointx = round(((this->lenght/2)/m)+this->center.x);

	} else
	{

	    startPointx = this->center.x - this->lenght/2;
		startPointy = round((m*-1*this->lenght/2)+this->center.y);
		endPointx = this->center.x + this->lenght/2;
		endPointy = round((m*this->lenght/2)+this->center.y);
	}

	cv::LineIterator it(input,cv::Point(startPointx,startPointy),cv::Point(endPointx,endPointy));
	
	this->linePoints.clear();

	for(int i = 0; i < it.count; i++, ++it)
	{
	    this->linePoints.push_back(it.pos());
	}
	
	int * orthoPoints = new int[4];

	orthoPoints = line::getOrthonomalLine(startPointx,startPointy,endPointx,endPointy);

	it = cv::LineIterator(input,cv::Point(orthoPoints[0],orthoPoints[1]),cv::Point(orthoPoints[2],orthoPoints[3]));
	
	this->orthon.clear();
	//punti ortonormali al centro
    for(int i = 0; i < it.count; i++, ++it)
	{
	    this->orthon.push_back(it.pos());
	}
		
	delete [] orthoPoints;
	return;
}


/**
Costruttore della classe lineOperator
@cv::Point2i center; punto in cui � centrato il fascio di rette astratto dalla classe
@int degrees; risoluzione espressa in gradi del fascio di rette 
@int lenght; lunghezza delle linee
*/
line::lineOperator::lineOperator ( cv::Point2i center, int degrees, int lenght)
{
    this->center = center;
	this->degreesStep = degrees;
	this->lenght = lenght;

	this->linesNumb = std::ceil(180/degrees);

}

/**

funzione che implementa la ricostruzione geodedica tramite opening e minimo pointwise con la maschera
@cv::Mat &inputImg; immagine di partenza
@cv::Mat &marker; maschera di partenza e risultato finale

*/
void utility::recostruction(const cv::Mat & inputImg, cv::Mat & marker)
{
	cv::Mat marker_prev;
	do
	{
		//copia di backup del precedente marker
		marker_prev = marker.clone();

		//dilatione geodetica ( = dilatazione + minimo pointwise con la maschera)
		cv::morphologyEx(marker, marker, CV_MOP_DILATE, cv::getStructuringElement(CV_SHAPE_ELLIPSE, cv::Size(3,3)));
		marker = cv::min(marker, inputImg);

			
	} while(cv::countNonZero(marker - marker_prev) > 0 );
}

/**

Sottrazione sfondo di un'immagine
@cv::Mat &input; immagine di partenza
@int size; dimensione del kernel
@bool light; sfondo chiaro (TRUE) sfondo scuro (FALSE)

*/
void utility::backSubtraction (cv::Mat & input, int size, bool light)
	{
	    cv::Mat kernel = cv::Mat::ones(size,size,CV_32F);
		cv::Mat background;
		kernel /= float(size*size);

		cv::filter2D(input,background,-1,kernel);

		input = (light) ? (input-background) : (background-input);

	}
