// include aia and ucas utility functions
#include "aiaConfig.h"


// include my project functions
#include "functions.h"

#include <opencv2\photo\photo.hpp>




int main() 
{

	//path
	std::string imagesPath = std::string(DATASET_PATH);
	std::string gtPath = std::string(DATASET_PATH);
	std::string maskPath = std::string(DATASET_PATH);

	std::string imagesExt;
	std::string gtExt;
	std::string maskExt;

	//PARAMETERS LINE OPERATOR
	int degreesStep;
	int lineLenght;
	int lineOpSmoothSize;
	int lineOpAreaSize;
	int lineOpBorderSize;
	

    //PARAMETERS MORPHOLOGY
	int		 nRot;
	cv::Size strElSize(9,47);
	int		 morphologyThreshold;
	int		 morpAreaSize;
	int		 morpBorderSize;

	//PARAMETERS OPTIC-DISK
	cv::Size claheSize(9,9);
	double	 clipLimit;
	int		 optDSmoothSize;
	int		 optDAreaSize;
	

	//OTHERS
	int radious;
    int hNoLocal;

	while(true)
	{
	try
    {
		char s;
		//utility::generateMask();
		printf("Scegli il dataset da elaborare:\n 1.DRIVE\n 2.STARE\n 3.CHASEDB1\n 4.ESCI\n");
	    s =(char) getchar();
		std::fflush(stdin);

		switch(s)
		{
		case '1':
			//drive
			imagesPath += "/DRIVE/images";
			imagesExt	= ".tif";

			gtPath += "/DRIVE/groundtruths";
			gtExt	= ".tif";

			maskPath += "/DRIVE/masks";
			maskExt	  = ".tif";

			//LINE OPERATOR
			degreesStep		 = 15;
			lineLenght		 = 15;
			lineOpSmoothSize = 111;
			lineOpAreaSize	 = 145;
			lineOpBorderSize = 23;
			line::service(3,50.0);

			//OPTIC DISK
			 clipLimit		= 3.0;
			 optDSmoothSize = 9;
			 optDAreaSize	= 65;
			

			//MORPHOLOGY
			 nRot				 = 36;
			 morphologyThreshold = 220;
			 morpAreaSize		 = 100;
			 morpBorderSize		 = 9;

			//OTHERS
			radious  = 60;
			hNoLocal = 3;

			break;

		case '2':
			//stare

			imagesPath += "/STARE/images";
			imagesExt	= ".ppm";

			gtPath += "/STARE/groundtruths";
			gtExt	= ".ppm";

			maskPath += "/STARE/masks";
			maskExt	  = ".png";

			//LINE OPERATOR
			degreesStep		 = 15;
			lineLenght		 = 15;
			lineOpSmoothSize = 111;
			lineOpAreaSize	 = 300;
			lineOpBorderSize = 47;
			line::service(2,50.0);

			//OPTIC DISK
			 clipLimit		 = 3.0;
			 optDSmoothSize  = 9;
			 optDAreaSize	 = 65;
			 

			//MORPHOLOGY
			 nRot = 36;
			 morphologyThreshold = 250;
			 morpAreaSize		 = 100;
			 morpBorderSize		 = 9;

			//OTHERS
			radious	 = 90;
			hNoLocal = 3;

		    break;

		case '3':

			//chasedb1

			imagesPath += "/CHASEDB1/images";
			imagesExt	= ".jpg";

			gtPath += "/CHASEDB1/groundtruths";
			gtExt	= ".png";

			maskPath += "/CHASEDB1/masks";
			maskExt	  = ".png";

			//LINE OPERATOR
			degreesStep		 = 15; 
			lineLenght		 = 25; 
			lineOpSmoothSize = 111; 
			lineOpAreaSize	 = 300;
			lineOpBorderSize = 25;
			line::service(3,65.0);

			//OPTIC DISK
			clipLimit	   = 3.0;
			optDSmoothSize = 9;
			optDAreaSize   = 65;

			//MORPHOLOGY
			morphologyThreshold = 250;
			morpBorderSize		= 5;
			morpAreaSize		= 250;
			nRot				= 18;  
			strElSize.width		= 27; 
			strElSize.height	= 33;  


			//OTHERS
			radious  = 100; 
			hNoLocal = 9; 
            
			

			break;

		case '4':

			return 0;
			break;

	    default:

			continue;
			break;

		}

		


        //std::cout<<std::string(DATASET_PATH);
		std::vector<cv::Mat> images = aia::getImagesInFolder(imagesPath,imagesExt);
		std::vector<cv::Mat> imgGTs = aia::getImagesInFolder(gtPath,gtExt,true);
		std::vector<cv::Mat> masks = aia::getImagesInFolder(maskPath,maskExt,true);
        

		std::vector<cv::Mat> segVec;
		std::vector<cv::Mat> gtVec;
		std::vector<cv::Mat> maskVec;
		std::vector<cv::Mat> visRes;
        
		
		for(int i=0;i<images.size();i++) {

			//apply the mask to the source image
			
			//images[i] = images[i] & masks[i];
			images[i].setTo(cv::Scalar(0),255-masks[i]);
			//aia::imshow("imamgine input mascherata",images[i]);git 
			//splitting RGB channels
			cv::Mat channels[3];
			cv::split(images[i], channels);
			cv::Mat greenChannel = channels[1].clone();
			//aia::imshow("greenChannel",greenChannel);

			//denoising with NonLocalMeans filter
			cv::Mat denoised;
			cv::fastNlMeansDenoising(greenChannel, denoised, hNoLocal,7,21);
			//aia::imshow("denoised",255-denoised);
			cv::Mat invertedDenoised = 255 - denoised;

			//ricerca profilo dei vasi tramite operazione morfologica --> TopHat
			cv::Mat topHat;
			utility::topHatTransform(invertedDenoised, topHat, nRot, strElSize);
			//aia::imshow("topHatoutput", topHat);

			//mascheramento
			topHat = topHat & masks[i];
		
			//si ricava un'immagine marker per avere un punto di partenza per la ricostruzione successiva
			cv::Mat marker;
			cv::morphologyEx(topHat, marker, CV_MOP_OPEN, cv::getStructuringElement(CV_SHAPE_RECT, cv::Size(3,3)));
			//aia::imshow("open",marker);

			//ricostruizione geodetica 
			utility::recostruction(topHat,marker);
			//aia::imshow("reconstruction",marker);

			//binarizzazione dell'immagine
			cv::threshold(marker,marker,morphologyThreshold,255,cv::THRESH_BINARY); 
	   		cv::Mat morphology = marker.clone();
			//aia::imshow("morph bin",morphology);

			//CLAHE
			//equalizzazione e correzione illuminazione tramite CLAHE
			cv::Mat diskExtraction = denoised.clone();
			cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(clipLimit,claheSize);
			clahe->apply(diskExtraction, diskExtraction);
			//aia::imshow("clahe",diskExtraction);
			//sottrazione sfondo
			cv::Mat equalized = diskExtraction.clone();
			utility::backSubtraction(diskExtraction,optDSmoothSize,false);
			
			//binarizzazione percentile
			cv::Mat opticDisk;
			utility::percentileBinarization(diskExtraction,opticDisk,90,100);

			//LINE OPERATOR
			cv::Mat lineResponse = cv::Mat(greenChannel.rows,greenChannel.cols,CV_32F,cv::Scalar(0));
			greenChannel = 255 - greenChannel;
			greenChannel.setTo(cv::Scalar(0),(255-masks[i]));
			//aia::imshow("green channel",greenChannel);
			
			//chiamata funzione principale lineOperator
			line::lineStrenght (greenChannel,lineResponse,degreesStep,lineLenght);
			
			//normalizzazione e conversione a CV_8U
			cv::normalize(lineResponse,lineResponse,0,255,cv::NORM_MINMAX);
			lineResponse.convertTo(lineResponse,CV_8U);
			
			//sottrazione dello sfondo
			utility::backSubtraction(lineResponse,lineOpSmoothSize,true);
			
			//binarizzazione tramite percentile
			cv::Mat lineOp;
			utility::percentileBinarization(lineResponse,lineOp,90,100);

			//Pulizia immagine con rimozione contorno fondo oculare e rimozione rumore piccole dimensioni
			//Per la morfologia viene fatta prima la rimozione del bordo perche interessati solo ai vasi di maggior dimensione

			utility::removeBorder(images[i], morpBorderSize, morphology); 
			//aia::imshow("morfologia SENZA BORDO",a);
			

			utility::removeSmallAreas(morphology,morpAreaSize);
			//aia::imshow("morfologia dopo di area",a); 
	
			utility::removeSmallAreas(opticDisk, optDAreaSize);
			//aia::imshow("smootihg after remov small areas",b); 
	
			utility::removeSmallAreas(lineOp,lineOpAreaSize);
			//aia::imshow("lineOper after remov small areas",lineOp);
	
			utility::removeBorder(images[i], lineOpBorderSize, lineOp);
			//aia::imshow("lineOperator SENZA BORDO",c);
			
			//aia::imshow("smoothing",opticDisk);
			//aia::imshow("lineOperator",lineOp);
			//aia::imshow("morfologia",morphology);

			//pulizia e definizione area disco ottico
			utility::computeDisk(equalized,radious,morphology,opticDisk,lineOp); 
			
			//elaborazione immagine di output 
			cv::Mat final = lineOp.clone();
			utility::addWithoutDisk(radious,equalized,morphology,lineOp,final); 
			//aia::imshow("final",final);

			//popolazione vettori per calcolo dell'accuracy
			segVec.push_back(final);
			gtVec.push_back(imgGTs[i]);
	        maskVec.push_back(masks[i]);

		/*	std::string str = std::to_string(i+1);
			if(i<9)
			str = "0" + str + "_result";
			else
			str = str + "_result";
			

	        ucas::imwrite(std::string(DATASET_PATH)+"/CHASEDB1/results/" + str +".png",final); */
		}
		//calcolo accuracy 
		printf("ACC: %f\n",aia::accuracy(segVec,gtVec,maskVec,&visRes));

		//visualizzazione risultati
		 for(int i = 0; i< visRes.size(); i++)
			aia::imshow("ACC",visRes[i]);   
		

        return 1;
	}
	
	catch (aia::error &ex)
	{
		std::cout << "EXCEPTION thrown by " << ex.getSource() << "source :\n\t|=> " << ex.what() << std::endl;
	}
	catch (ucas::Error &ex)
	{
		std::cout << "EXCEPTION thrown by unknown source :\n\t|=> " << ex.what() << std::endl;
	}

}
}